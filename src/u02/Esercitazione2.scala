package u02

object Esercitazione2 extends App {
  val parity:Int  => String  = {
    case n if n%2==0  => "even"
    case _ => "odd"
  }
//3a
  def parity2(n: Int) : String = n%2 match{
    case 0  => "even"
    case _ => "odd"
  }

//3b
  def neg(p: String=>Boolean) :(String=>Boolean)=
    (x:String) => !p(x)

  val neg2:(String=>Boolean)=>(String=>Boolean)=
    p=> s=> !p(s)
//3c
  def negG[A](p: A=>Boolean) :(A=>Boolean)=
    (x:A) => !p(x)

//  val empty: String => Boolean = _=="" // predicate on strings
//  val notEmpty = negG(empty) // which type of notEmpty?
//  print(notEmpty("foo"))// true
//  print(notEmpty("") )// false
//  print(notEmpty("foo") && !notEmpty("") )// true.. a comprehensive test

//  val zero: Int=>Boolean = _==0
//  val notZero = negG(zero)
//  print(notZero(2))
//  print(notZero(0))
//  print(notZero(2)&& !notZero(0))

//4
  val p1: Int => Int => Int => Boolean = x => y => z => (x<=y) && (y<=z)
  val p2: (Int,Int,Int)=> Boolean = (x,y,z) => (x<=y) && (y<=z)
  def p3(x: Int)(y: Int)(z: Int): Boolean = (x<=y) && (y<=z)
  def p4(x: Int, y: Int, z: Int): Boolean = (x<=y) && (y<=z)

//  println(p1(1)(2)(3))//true
//  println(p2(1,2,3))//true
//  println(p3(1)(2)(1))//false
//  println(p4(1,2,1))//false


  //5
  def compose(f: Int => Int, g: Int => Int): Int => Int= (x:Int) => f(g(x))
  //println(compose(_-1,_*2)(5)) // 9


  def composeG[A](f: A => A, g: A => A): A => A=  (x:A) => f(g(x))

  //println(composeG[Int](_-1,_*2)(5)) // 9

  //6
  def fib(n:Int): Int = n match{
    case 0 => 0
    case 1 => 1
    case _ => fib(n-1)+fib(n-2)
  }
  //println((fib(0),fib(1),fib(2),fib(3),fib(4)))

  def fibTR(n: Int):Int= {
    @annotation.tailrec
    def _fib(n:Int,acc:Int,b:Int): Int = n match{
      case 0 => acc
      case 1 => b
      case _ => _fib(n-1,b,acc+b)
    }
    _fib(n,0,1)
  }
  println((fibTR(0),fibTR(1),fibTR(2),fibTR(3),fibTR(4)))
}